require 'rspec/core/rake_task'

ENV['LANG']="C.UTF-8"

RSpec::Core::RakeTask.new(:spec) do |spec|
 spec.pattern      = './spec/*_spec.rb'
end

task :default => :spec
